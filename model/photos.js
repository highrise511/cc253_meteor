Photos = new Mongo.Collection("photos");
//[ 'admin', 'user-admin' ]
Photos.allow({
  insert: function (userId, photo) {
    return userId && photo.owner === userId;
  },
  update: function (userId, photo, fields, modifier) {
    if (userId !== photo.owner && Meteor.user().roles.indexOf('admin') < 0)
      return false;

    return true;
  },
  remove: function (userId, photo) {
    if (userId !== photo.owner && Meteor.user().roles.indexOf('admin') < 0)
      return false;

    return true;
  }
});
