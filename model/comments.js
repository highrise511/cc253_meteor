Comments = new Mongo.Collection("comments");

Comments.allow({
  insert: function (userId, comment) {
	  //Meteor.user().roles.indexOf('admin') >= 0
    return userId && comment.owner === userId;
  },
  update: function (userId, comment, fields, modifier) {
    if (userId !== comment.owner && Meteor.user().roles.indexOf('admin') < 0)
      return false;

    return true;
  },
  remove: function (userId, comment) {
    if (userId !== comment.owner && Meteor.user().roles.indexOf('admin') < 0)
      return false;

    return true;
  }
});

Meteor.methods({
  updateCommentLikes: function(cId) {
	var uId = Meteor.userId();
	if(uId){
		if(isLiked(cId,uId)){
			Comments.update({ 
		        "_id": cId, 
		        "likes": uId
		    },
		    {
		        "$inc": { "likeCount": -1 },
		        "$pull": { "likes": uId }
		    });
		}else{		
		    Comments.update({ 
		        "_id": cId, 
		        "likes": { "$ne": uId }
		    },
		    {
		        "$inc": { "likeCount": 1 },
		        "$push": { "likes": uId }
		    });
		}  
	}
  }
});

var isLiked = function(cId, uId){
	var fc = Comments.find(
	    { 
	    	"_id": cId, 
	    	"likes": uId 
	    }
	)
	return fc.count() > 0;
}
