Meteor.publish("comments", function (options, searchString) {
  if (searchString == null)
    searchString = '';
  Counts.publish(this, 'numberOfComments', Comments.find({
    'blogId' : { '$regex' : '.*' + searchString || '' + '.*', '$options' : 'i' },
    $or:[
      {$and:[
        {owner: this.userId},
        {owner: {$exists: true}}
      ]}
  ]}), { noReady: true });
  return Comments.find({
    'blodId' : { '$regex' : '.*' + searchString || '' + '.*', '$options' : 'i' },
    $or:[
      {$and:[
        {owner: this.userId},
        {owner: {$exists: true}}
      ]}
    ]} ,options);
});