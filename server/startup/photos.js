Meteor.publish("photos", function (options, searchString) {
  if (searchString == null)
    searchString = '';
  Counts.publish(this, 'numberOfPhotos', Photos.find({
    'name' : { '$regex' : '.*' + searchString || '' + '.*', '$options' : 'i' },
    }), { noReady: true });
  return Photos.find({
    'name' : { '$regex' : '.*' + searchString || '' + '.*', '$options' : 'i' },
    } ,options);
});