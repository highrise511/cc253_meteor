(function () {
	
	angular.module('interviews',[
	 'angular-meteor',
	 'ui.router',
	 'angularUtils.directives.dirPagination',
	 'uiGmapgoogle-maps',
	 'ngMaterial'])
	.config(['$mdThemingProvider',function($mdThemingProvider) {
	  $mdThemingProvider
	  	.theme('default')
	  	.accentPalette('orange')
	    .backgroundPalette('grey');
	}]);
	angular.module('interviews').config(['$mdIconProvider', function ($mdIconProvider) {
		  $mdIconProvider
		    .iconSet("social", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-social.svg")
		    .iconSet("action", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-action.svg")
		    .iconSet("communication", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-communication.svg")
		    .iconSet("content", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-content.svg")
		    .iconSet("toggle", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-toggle.svg")
		    .iconSet("navigation", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-navigation.svg")
		    .iconSet("image", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-image.svg");
		}]);
	angular.module('interviews').factory('cf', ['$meteor', '$location','$sce', '$mdMedia', 
	function($meteor, $location, $sce, $mdMedia) {
		return {
			go : function ( path ) {
			  	  $location.path( path );
		  	},
			trustSrc : function(src) {
			    	return $sce.trustAsResourceUrl(src);
		    },
		    isAdmin : function() {
		    	if(Meteor.user() && Meteor.user().roles && Meteor.user().roles.indexOf('admin') >= 0){
		    		return true;
	    		}
		    	
		    }
		}
	}]);
	angular.module('interviews')
	.controller('gc', ['$scope', '$meteor', '$location','$sce','$mdMedia',
	function($scope, $meteor, $location, $sce, $mdMedia){
		var isAdmin = function() {
			if(Meteor.user() && Meteor.user().roles && Meteor.user().roles.indexOf('admin') >= 0){
				return true;
			}
		}
		$scope.go = function ( path ) {
		  	  $location.path( path );
		},
		$scope.trustSrc = function(src) {
		    	return $sce.trustAsResourceUrl(src);
		},
		$scope.showAdd = function(){
	    	return isAdmin();
	    }
		$scope.isAdmin = function() {
			return isAdmin();
		}
		//leaving for future resize event handling template!
	    $scope.$watch(function() { return $mdMedia('sm'); }, function(big) {
//	        console.log('sm');
	      });
	    $scope.$watch(function() { return $mdMedia('md'); }, function(big) {
//	        console.log('md');
	      });
	    $scope.$watch(function() { return $mdMedia('lg'); }, function(big) {
//	        console.log('lg');
	      });
	}]);
	Meteor.subscribe('userData');
	function onReady() {
	  angular.bootstrap(document, ['interviews']);
	}
})();