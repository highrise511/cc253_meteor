angular.module("interviews").config(['$urlRouterProvider', '$stateProvider', '$locationProvider',
  function($urlRouterProvider, $stateProvider, $locationProvider){

    $locationProvider.html5Mode(true);

    $stateProvider
    .state('home', {
        url: '/creatingconversation',
        templateUrl: 'client/blogs/views/blog-home.ng.html',
        controller: 'BlogHomeCtrl'
      })	
      .state('parties', {
        url: '/parties',
        templateUrl: 'client/parties/views/parties-list.ng.html',
        controller: 'PartiesListCtrl'/*, 
        resolve: { 
          'subscribe': [ '$meteor', function($meteor) { 
            return $meteor.collection(Parties).subscribe('parties'); 
          }] 
        }*/
      })
      .state('partyDetails', {
        url: '/parties/:partyId',
        templateUrl: 'client/parties/views/party-details.ng.html',
        controller: 'PartyDetailsCtrl'
      })
      .state('partyDetailsEdit', {
        url: '/parties/edit/:partyId',
        templateUrl: 'client/parties/views/party-details-edit.ng.html',
        controller: 'PartyDetailsCtrl',
        resolve: {
          "currentUser": ["$meteor", function($meteor){
            return $meteor.requireUser();
          }]
        }
      })
      .state('conversations', {
        url: '/conversations',
        templateUrl: 'client/blogs/views/blog-list.ng.html',
        controller: 'BlogListCtrl'/*, 
        resolve: { 
          'subscribe': [ '$meteor', function($meteor) { 
            return $meteor.collection(Parties).subscribe('parties'); 
          }] 
        }*/
      })
      .state('blogDetails', {
        url: '/conversations/:blogId',
        templateUrl: 'client/blogs/views/blog-details.ng.html',
        controller: 'BlogDetailsCtrl'
      })
      .state('blogEdit', {
        url: '/conversations/edit/:blogId',
        templateUrl: 'client/blogs/views/blog-details-edit.ng.html',
        controller: 'BlogDetailsCtrl',
        resolve: {
          "currentUser": ["$meteor", function($meteor){
            return $meteor.requireUser();
          }]
        }
      })
      .state('profiles', {
        url: '/profiles',
        templateUrl: 'client/profiles/views/profile-list.ng.html',
        controller: 'ProfileListCtrl'/*, 
        resolve: { 
          'subscribe': [ '$meteor', function($meteor) { 
            return $meteor.collection(Parties).subscribe('parties'); 
          }] 
        }*/
      })
      .state('profileDetails', {
        url: '/profiles/:profileId',
        templateUrl: 'client/profiles/views/profile-details.ng.html',
        controller: 'ProfileDetailsCtrl'
      })
      .state('profileEdit', {
        url: '/profiles/edit/:profileId',
        templateUrl: 'client/profiles/views/profile-edit.ng.html',
        controller: 'ProfileDetailsCtrl',
        resolve: {
          "currentUser": ["$meteor", function($meteor){
            return $meteor.requireUser();
          }]
        }
      })
      .state('photoList', {
        url: '/gallery',
        templateUrl: 'client/photos/views/photos-list.ng.html',
        controller: 'PhotosListCtrl'
      })
      .state('about', {
        url: '/about',
        templateUrl: 'client/about/views/about-details.ng.html'
      });

    $urlRouterProvider.otherwise("/creatingconversation");
}]);
  
angular.module("interviews").run(["$rootScope", "$state", function($rootScope, $state) {
	
	$rootScope.$on('$stateChangeSuccess', 
		function(event, toState, toParams, fromState, fromParams){ 
	    var cc = document.getElementById('ccContent');
	    cc.scrollIntoView();
	    cc.scrollTop=0;
	});
  $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
    // We can catch the error thrown when the $requireUser promise is rejected
    // and redirect the user back to the main page
    if (error === "AUTH_REQUIRED") {
      $state.go('home');
    }
  });
}]);
