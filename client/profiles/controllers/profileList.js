angular.module("interviews").controller("ProfileListCtrl", ['$scope', '$meteor', '$location', '$mdDialog', '$window',
  function($scope, $meteor, $location, $mdDialog, $window){

    $scope.profiles = $meteor.collection(Profiles).subscribe('profiles');

    $scope.remove = function(profile){
      if($scope.confirmDelete())$scope.profiles.splice( $scope.profiles.indexOf(profile), 1 );
    };

    $scope.removeAll = function(){
      $scope.profiles.remove();
    };
    
    $scope.showAdvanced = function(ev) {
        $mdDialog.show({
          controller: DialogController,
          templateUrl: 'client/profiles/views/newProfileDialog.ng.html',
          targetEvent: ev,
        })
        .then(function(newProfile) {
        	$scope.profiles.push(newProfile);
        	console.log('saved new profile');
        }, function() {
        	console.log('cancelled new post');
        });
      };
}]);

angular.module("interviews").filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});	

function DialogController($scope, $mdDialog, $meteor) {
	$scope.Date = new Date();
	$scope.hide = function() {
		$mdDialog.hide();
	};
	$scope.cancel = function() {
	  $mdDialog.cancel();
	};
	$scope.answer = function(newProfile) {
	  $mdDialog.hide(newProfile);
	};
}
DialogController.$inject = ['$scope', '$mdDialog', '$meteor']