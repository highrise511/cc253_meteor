angular.module("interviews").controller("ProfileDetailsCtrl", ['$scope', '$stateParams', '$meteor',
  function($scope, $stateParams, $meteor){

   $scope.users = $meteor.collection(Meteor.users, false).subscribe('users');
   $scope.profile = $meteor.object(Profiles, $stateParams.profileId).subscribe('profiles');
   
}]);
