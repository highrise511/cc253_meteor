angular.module("interviews").controller("AboutCtrl", ['$scope', '$meteor', '$mdMedia', '$location', '$mdDialog',
  function($scope, $meteor, $mdMedia, $location, $mdDialog){

    $scope.go = function ( path ) {
    	  $location.path( path );
	};
    
    $scope.showAdvanced = function(ev) {
    	console.log('show dialog');
    	$mdDialog.show({
    		controller: AboutDialogController,
    		templateUrl: 'client/about/views/aboutDialog.ng.html',
    		targetEvent: ev,
    	})
    	.then(function() {
    		console.log('do work son!');
    	}, function() {
        	console.log('cancelled new post');
        });
      
      }
    var imageTallPath = 'bgs/ccFlyerTall.jpeg';

}]);

function AboutDialogController($scope, $mdDialog, $meteor) {
	$scope.Date = new Date();
	$scope.hide = function() {
		$mdDialog.hide();
	};
	$scope.cancel = function() {
	  $mdDialog.cancel();
	};
	$scope.answer = function(newBlog) {
	  $mdDialog.hide(newBlog);
	};
}
AboutDialogController.$inject = ['$scope', '$mdDialog', '$meteor', '$sce']