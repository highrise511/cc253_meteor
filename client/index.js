angular
  .module('interviews')
  .controller('AppCtrl', ['$scope', '$timeout', '$mdSidenav', '$mdUtil', '$log', '$mdDialog','$location',
		  function ($scope, $timeout, $mdSidenav, $mdUtil, $log, $mdDialog, $location) {
    //scroll page to top;
	$scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');
    $scope.Date = new Date();
    
    $scope.go = function ( path ) {
  	  $location.path( path );
	};
	
	$scope.getSmImgUrl = function(imgName) {
    	return "http://demo.cloudimg.io/s/height/150/http://creatingconvo.com/" + imgName;
    }
	$scope.getMdImgUrl = function(imgName) {
		var m = "http://demo.cloudimg.io/s/height/400/http://creatingconvo.com/" + imgName;
    	return m
    }
	$scope.getLgImgUrl = function(imgName) {
    	return "http://demo.cloudimg.io/s/height/700/http://creatingconvo.com/" + imgName;
    }
	
	$scope.confirmDelete = function(){
	    var x;
	    if (confirm("Are You Sure?") == true) {
	        x = true;
	    } else {
	        x = false;
	    }
	    return x;
	};
	
	$scope.getMRBanner = function() {
    	return "http://demo.cloudimg.io/s/width/800/http://creatingconvo.com/bgs/mr_bg.jpg";
    }
	
	$scope.showAdd = function(){
    	return isAdmin();
    }
	$scope.canRemove = function(item){
		if(Meteor.user() && Meteor.user().roles && Meteor.user().roles.indexOf('admin') >= 0){
			return true;
		}
		return true;
	}
	var isAdmin = function() {
		if(Meteor.user() && Meteor.user().roles && Meteor.user().roles.indexOf('admin') >= 0){
			return true;
		}
	}
	$scope.isAdmin = function() {
		return isAdmin();
	}
	
	$scope.openLeft = function () {
        $mdSidenav('left').open()
          .then(function () {
            //$log.debug("close LEFT is done");
          });
      };
	
    /**
     * Build handler to open/close a SideNav; when animation finishes
     * report completion in console
     */
    function buildToggler(navID) {
      var debounceFn =  $mdUtil.debounce(function(){
            $mdSidenav(navID)
              .toggle()
              .then(function () {
                //$log.debug("toggle " + navID + " is done");
              });
          },300);
      return debounceFn;
    }
  }])
  .controller('LeftCtrl', ['$scope', '$timeout', '$mdSidenav', '$log',
           function ($scope, $timeout, $mdSidenav, $log) {
    $scope.close = function () {
      $mdSidenav('left').close()
        .then(function () {
          //$log.debug("close LEFT is done");
        });
    };
    
    $scope.menu = [
                   {
                     link : '',
                     title: 'Home',
                     icon: 'dashboard'
                   },
                   {
                     link : '',
                     title: 'Episodes',
                     icon: 'group'
                   },
                   {
                     link : '',
                     title: 'Gallery',
                     icon: 'message'
                   },
                   {
                     link : '',
                     title: 'Crew',
                     icon: 'message'
                   },
                   {
                     link : '',
                     title: 'About',
                     icon: 'message'
                   }
                 ];
                 $scope.admin = [
                   {
                     link : '',
                     title: 'Parties',
                     icon: 'delete'
                   },
                   {
                     link : 'showListBottomSheet($event)',
                     title: 'Settings',
                     icon: 'settings'
                   }
                 ];
    
    
  }])
  .controller('RightCtrl', ['$scope', '$timeout', '$mdSidenav', '$log',
            function ($scope, $timeout, $mdSidenav, $log) {
    $scope.close = function () {
      $mdSidenav('right').close()
        .then(function () {
          //$log.debug("close RIGHT is done");
        });
    };
  }]);

Accounts.ui.config({
	passwordSignupFields: 'USERNAME_AND_EMAIL'
	});