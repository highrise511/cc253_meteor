angular.module("interviews").controller("BlogHomeCtrl", ['$scope','$rootScope','$meteor', '$sce', '$mdMedia', '$location', '$mdDialog',
  function($scope, $rootScope, $meteor, $sce, $mdMedia, $location, $mdDialog){

	$scope.blogs = $meteor.collection(Blogs).subscribe('blogs');
	$meteor.subscribe('users');
	//var defaultLink = "https://www.youtube.com/embed/i-b683mcwC8";
//	var defaultLink = "https://www.youtube.com/embed/YaLb93scUho?loop=1";
//	var defaultLink = "https://www.youtube.com/embed/5i4syqyTzAI?loop=1";
	var defaultLink = "https://www.youtube.com/embed/t6UJ9btRaRY?loop=1";
	var bId = 2;
		
	$scope.blog = {
			name:"2015 Post Production Reel CC - Creating Conversation", 
			link:defaultLink
			};
	$scope.mainurl= $scope.getReactively('blog.link');
	$scope.fBlog = {
			name:"2015 Post Production Reel CC - Creating Conversation", 
			link:defaultLink
			};
	$scope.mainurl= $scope.getReactively('blog.link');
    
    //url(http://demo.cloudimg.io/s/width/500/http://creatingconvo.com/hmPhotos/01.JPG)
    $scope.getSmallImgUrl = function(imgName) {
    	return "http://demo.cloudimg.io/s/height/400/http://creatingconvo.com/" + imgName;
    }
    
    $scope.addComment=function(newComment, root){
    	newComment.blogId=bId;
    	newComment.owner=root.currentUser._id;
    	newComment.date=new Date();
    	newComment.likes=[];
    	newComment.likeCount=0;
    	$scope.comments.push(newComment);
    	$scope.newComment='';
    }
    
    $scope.likeComment = function(comment){
    	Meteor.call('updateCommentLikes', comment._id);
    };
    
    $scope.go = function ( path ) {
  	  $location.path( path );
  	};
  	
    $scope.trustSrc = function(src) {
    	return $sce.trustAsResourceUrl(src);
    }

    //leaving for future resize event handling template!
    $scope.$watch(function() { return $mdMedia('sm'); }, function(small) {
        console.log('sm');
      });
    $scope.$watch(function() { return $mdMedia('md'); }, function(med) {
        console.log('md');
      });
    $scope.$watch(function() { return $mdMedia('lg'); }, function(lrg) {
        console.log('lg');
      });

    
    $scope.showAdvanced = function(ev) {
    	console.log('show dialog');
        $mdDialog.show({
          controller: DialogController,
          templateUrl: 'client/blogs/views/newBlogDialog.ng.html',
          targetEvent: ev,
        })
        .then(function(newBlog) {
        	$scope.blogs.push(newBlog);
        	console.log('saved new blog');
        }, function() {
        	console.log('cancelled new post');
        });
      };
      
      $scope.page = 1;
      $scope.perPage = 100;
      $scope.sort = { date: 1 };
      $scope.orderProperty = '1';
      
      $scope.search = bId;

      $scope.pageChanged = function(newPage) {
          $scope.page = newPage;
        };
        
      $scope.comments = $meteor.collection(function() {
          return Comments.find({blogId:bId}, {
            sort : $scope.getReactively('sort')
          });
        });
       $scope.remove = function(comment){
	      if($scope.confirmDelete())$scope.comments.splice( $scope.comments.indexOf(comment), 1 );
	    };

	    $scope.getUserById = function(userId){
	      return Meteor.users.findOne(userId);
	    };

	    $scope.creator = function(comment){
	      if (!comment)
	        return;
	      var owner = $scope.getUserById(comment.owner);
	      if (!owner)
	        return "nobody";

	      if ($rootScope.currentUser)
	        if ($rootScope.currentUser._id)
	          if (owner._id === $rootScope.currentUser._id)
	            return "me";

	      return owner;
	    };
	    
	    $scope.creator = function(comment){
		    if (!comment)
		      return;
		    var owner = $scope.getUserById(comment.owner);
		    if (!owner)
		      return "nobody";
		
		    if ($rootScope.currentUser)
		      if ($rootScope.currentUser._id)
		        if (owner._id === $rootScope.currentUser._id)
		          return "me";
		
		    return owner;
		  };
   
   $meteor.autorun($scope, function() {
	      $meteor.subscribe('comments', {
	        limit: parseInt($scope.getReactively('perPage')),
	        skip: (parseInt($scope.getReactively('page')) - 1) * parseInt($scope.getReactively('perPage')),
	        sort: $scope.getReactively('sort')
	      }, $scope.getReactively('search')).then(function() {
	        $scope.commentsCount = $meteor.object(Counts ,'numberOfComments', false);
	        
	      });
	    });
}]);

angular.module("interviews").filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});

function DialogController($scope, $mdDialog, $meteor, $sce) {
	$scope.Date = new Date();
	$scope.hide = function() {
		$mdDialog.hide();
	};
	$scope.cancel = function() {
	  $mdDialog.cancel();
	};
	$scope.answer = function(newBlog) {
	  $mdDialog.hide(newBlog);
	};
	$scope.trustSrc = function(src) {
	  return $sce.trustAsResourceUrl(src);
	}
}
DialogController.$inject = ['$scope', '$mdDialog', '$meteor', '$sce']