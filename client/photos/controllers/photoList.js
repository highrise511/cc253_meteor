angular.module("interviews").controller("PhotosListCtrl", ['$scope', '$stateParams', '$meteor', '$rootScope', '$state',
  function($scope, $stateParams, $meteor, $rootScope, $state){

	$scope.photos = $meteor.collection(Photos).subscribe('photos');
	
}]);